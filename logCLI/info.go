package logCLI

// Info will print the log to stdout and return an indented log
func (l *LogCLI) Info(s string, params ...interface{}) *LogCLI {
	return l.baseLogCLI(infoPrefix, s, params...)
}
