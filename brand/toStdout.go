package brand

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/util"
)

// ToStdout will print the Löxe ASCII art to the stdout.
// If ANSI codes are supported by the stdout, the output will
// be colored
//
// Note that is possible to provide a subtitle as an optional
// argument
func ToStdout(supportsANSI bool, subtitle ...string) {
	text := brandAsciiArt
	if len(subtitle) != 0 {
		text = WithSubtitle(subtitle[0])
	}

	if supportsANSI {
		fmt.Println(util.BoldPurpleString(text))
	} else {
		fmt.Println(text)
	}
}
