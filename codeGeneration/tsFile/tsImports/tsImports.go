package tsImports

// tsImport represents a single Typescript - TS
// import line to a single path
type tsImport struct {
	path          string
	defaultImport string
	namedImports  []string
}

// TsImports is a collection of single
// Typescript - TS line imports
type TsImports []*tsImport

// NewTsImports will just return a pointer to
// an empty TsImports
func NewTsImports() *TsImports {
	imports := make(TsImports, 0)
	return &imports
}
