package goParser

import (
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"go/types"
)

type FileInterfacesIterator = func(struct_ *types.TypeName, parentLog *logCLI.LogCLI) error

// IterateFileInterfaces will iterate only over the interfaces that are defined
// inside the parsed files
func (p *GoParser) IterateFileInterfaces(callback FileInterfacesIterator) error {
	fileTypeNamesIterator := func(type_ *types.TypeName, parentLog *logCLI.LogCLI) error {
		log := parentLog.Debug("Analysing *types.TypeName '%s'...", type_.Name())
		_, isStruct := type_.Type().Underlying().(*types.Interface)
		if !isStruct {
			log.Debug("Skipped (not a interface)...")
			return nil
		}

		e := callback(type_, log)
		if e != nil {
			return e
		}

		return nil
	}
	return p.iterateFileTypeNames(fileTypeNamesIterator)
}
