package goParser

import (
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"go/ast"
	"golang.org/x/tools/go/packages"
)

type packageFilesIterator = func(currFile *ast.File, filePkg *packages.Package, parentLog *logCLI.LogCLI) error

// iteratePackageFiles will iterate over the files inside the parsed packages.
//
// Note that if the focus is set to filepath, it will iterate only over the specified
// file
func (p *GoParser) iteratePackageFiles(callback packageFilesIterator) error {
	packagesIterator := func(filePkg *packages.Package, parentLog *logCLI.LogCLI) error {
		for _, currFile := range filePkg.Syntax {
			currFilePath := p.fileSet.File(currFile.Pos()).Name()
			log := parentLog.Debug("Analysing *ast.File '%s'...", currFilePath)

			if !p.focus.is(focusFilePath, currFilePath) {
				log.Debug("Skipped (not the focus)...")
				continue
			}

			e := callback(currFile, filePkg, log)
			if e != nil {
				return e
			}
		}

		return nil
	}
	return p.iteratePackages(packagesIterator)
}
