package goParser

import (
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"golang.org/x/tools/go/packages"
)

type packagesIterator = func(currPkg *packages.Package, parentLog *logCLI.LogCLI) error

// iteratePackages will iterate over the parsed packages.
//
// Note that if the focus is set to packagePath, it will iterated only over
// the specified package
func (p *GoParser) iteratePackages(callback packagesIterator) error {
	for _, currPkg := range p.pkgs {
		log := p.log.Debug("Analysing *packages.Package '%s %s'...", currPkg.Name, currPkg.PkgPath)

		if len(currPkg.Errors) != 0 {
			errorsLog := log.Error("This package contain errors. Skipping it...")
			for _, currError := range currPkg.Errors {
				errorsLog.Error(currError.Error())
			}
			continue
		}
		if !p.focus.is(focusPackagePath, currPkg.PkgPath) {
			log.Debug("Skipped (not the focus)...")
			continue
		}

		e := callback(currPkg, log)
		if e != nil {
			return e
		}
	}

	return nil
}
