package goParser

import (
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"go/token"
	"golang.org/x/tools/go/packages"
)

// Config holds some information about the
// parser behaviour.
//
// Some fields of the packages.Config struct
// may appear inside this Config struct, exposing
// them to client customization
type Config struct {
	Tests      bool
	Dir        string
	Env        []string
	BuildFlags []string
	Focus      *ParserFocus
	Fset       *token.FileSet
}

// packagesLoadConfig is the configuration of the packages.Load function
func packagesLoadConfig(config Config, log *logCLI.LogCLI) *packages.Config {
	if config.Fset == nil {
		config.Fset = token.NewFileSet()
	}

	mode := packages.NeedImports | packages.NeedSyntax | packages.NeedName | packages.NeedTypes | packages.NeedTypesInfo
	return &packages.Config{
		// Customizable configurations
		Env:        config.Env,
		BuildFlags: config.BuildFlags,
		Tests:      config.Tests,
		Dir:        config.Dir,
		Fset:       config.Fset,

		// Fixed value, don't expose
		Mode: mode,
		Logf: func(format string, args ...interface{}) { log.Debug(format, args...) },

		// Not used
		Context:   nil,
		ParseFile: nil,
		Overlay:   nil,
	}
}
